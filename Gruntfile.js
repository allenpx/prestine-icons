
var pkg = require('./package');

module.exports = function(grunt) {

    var liveReloadPort = 6212;

    grunt.initConfig({

        icons: {
            build: {
                icons:['source/*.svg'],
                namespace:'icon',
                lessDest:'less/icons.less',
                iconsDest:'images/icons',
                variants:[
                    {
                        'name':'color',
                        'primary':"#666BC7",
                        'secondary':"#E24B5C"
                    },
                    {
                        'name':'white',
                        'primary':"#ffffff",
                        'secondary':"#ffffff"
                    },
                    {
                        'name':'black',
                        'primary':"#black",
                        'secondary':"#black"
                    },
                    {
                        'name':'primary',
                        'primary':"#0469B1",
                        'secondary':"#0469B1"
                    },
                    {
                        'name':'secondary',
                        'primary':"#67B6BD",
                        'secondary':"#67B6BD"
                        //'secondary':"#00BDCF"
                    },
                    {
                        'name':'tertiary',
                        'primary':"#FAC537",
                        'secondary':"#FAC537"
                    }
                ]
            }
        },

        filelist:{
            getIcons:{
                src: [
                    'source/*.svg',
                ],
                dest: 'icons.json',
                intro:'{"icons":[',
                transform:function(pathObj){
                    return '{"name":"'+pathObj.name.replace('icon_','')+'"}';
                },
                delimeter:',\n',
                outro:']}'
            },
        },

        svgmin: {
            options: {
                plugins: [
                    { removeViewBox: false },
                    { removeUselessStrokeAndFill: false }
                ]
            },
            icons: {
                files: [{
                    expand:true,
                    cwd: 'images/icons',
                    dest: 'images/icons',
                    src: ['*.svg'],
                    ext: '.svg'
                }]
            }
        },

        less: {
            dev: {
                options: {
                    paths: [
                        "less",
                        "node_modules",
                    ],
                    compress: false,
                    sourceMap:false,
                    strictImports:true,
                },
                files: {
                    "css/icons.css":  "less/build.less"
                }
            },
        },

        mustache_render: {
            index:{
                options:{

                },
                files:[
                    {
                        data: "icons.json",
                        template: "index.mustache",
                        dest: "index.html"
                    }
                ]

            }
        },

        watch:{
            less: { 
                files: ['less/**/*.less', '!less/config/modules.less'],
                tasks: ['filelist','less'],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
            index: { 
                files: ['index.mustache'],
                tasks: ['mustache_render'],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
            icons: { 
                files: ['source/*.svg'],
                tasks: ['icons','svgmin','filelist', 'less', 'mustache_render'],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
        }
        

    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-svgmin');
    grunt.loadNpmTasks('grunt-tasks');
    grunt.loadNpmTasks('grunt-mustache-render');
    grunt.registerTask('default', [
        'icons',
        'svgmin',
        'less',
        'mustache_render',
        'watch',
    ]);

};
