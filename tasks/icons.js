var fs = require('fs');
var _ = require('underscore');
var path = require('path');


module.exports = function (grunt) {

    grunt.registerMultiTask('icons', 'colorize svg icons', function() {

        var path = require('path');
        var done = this.async();

        var self = this;
        var data = this.data;

        data.defaultPrimary = data.defaultPrimary || "#111111";
        data.defaultSecondary = data.defaultSecondary || "#ff0000";

        var primaryRegex = new RegExp(data.defaultPrimary, "gi");
        var secondaryRegex = new RegExp(data.defaultSecondary, "gi");

        var iconCSS = "";
        var colorFormatRegex = /\$name/g;

        var sourceIcons = grunt.file.expand(data.icons);

        //create the variant regex
        data.variants.forEach(function(variant){
            if(variant.color){
                variant.colorRegex = new RegExp(variant.color, "gi");
            }
            if(variant.primary){
                variant.primaryRegex = new RegExp(variant.primary, "gi");
            }
            if(variant.secondary){
                variant.secondaryRegex = new RegExp(variant.secondary, "gi");
            }
        });

        //apply the variants and the write the icons
        sourceIcons.forEach(function(filePath){

            var file = grunt.file.read(filePath);
            var fileName = path.basename(filePath, ".svg").replace("icon_", "");

            iconCSS += data.cssFormat.replace(colorFormatRegex, fileName);

            data.variants.forEach(function(variant){
                var newFile = "";
                var newFileName = fileName+ ( (variant.name && variant.name != 'default')?"-"+variant.name:'') +'.svg';
                if(variant.color){
                    newFile = file.replace(primaryRegex, variant.color).replace(secondaryRegex, variant.color);
                }else{
                    newFile = file.replace(primaryRegex, variant.primary).replace(secondaryRegex, variant.secondary);
                }
                grunt.file.write(data.iconsDest+'/'+newFileName, newFile);
            },this);


        },this);

        iconCSS = '@import "less-mixins/less-mixins.less";\n\n'+iconCSS;

        grunt.file.write(data.lessDest, iconCSS);

        done();

    });



};